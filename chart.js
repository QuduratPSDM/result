const radarData = {
    labels: ['Math & Comparison', 'Logic', 'Shapes', 'Language Proficiency', 'Correspondence', 'Completion'],
    datasets: [
        {
            label: 'Anda',
            data: [100, 100, 100, 100, 100, 100],
            backgroundColor: '#3f9fd632',
            borderColor: '#3f9fd6',
            borderWidth: 1
        },
        {
            label: 'Rata-rata',
            data: [48.1, 44.9, 55.1, 37.7, 44, 42.8],
            backgroundColor: 'rgb(14, 169, 97, 0.2)',
            borderColor: 'rgb(14, 169, 97, 1)',
            borderWidth: 1
        }
    ]
};

const radarOptions = {
    responsive: true,
    scales: {
        r: {
            suggestedMin: 0,
            suggestedMax: 100,
        }
    },
    plugins: {
        legend: {
            position: 'bottom',
        }
    }
};

const barData = {
    labels: ['1', '2', '3', '4', '5', '6'],
    datasets: [
        {
            label: 'Nilai',
            data: [15, 14, 12, 16, 15, 14],
            backgroundColor: '#3f9fd632',
            borderColor: '#3f9fd6',
            borderWidth: 1
        },
        {
            label: 'Rata-Rata',
            data: [10.4, 9.8, 8.7, 9.9, 9.2, 10.6],
            backgroundColor: 'rgb(14, 169, 97, 0.2)',
            borderColor: 'rgb(14, 169, 97, 1)',
            borderWidth: 1
        }
    ]
};

const barOptions = {
    scales: {
        y: {
            beginAtZero: true,
            max: 20
        }
    },
    plugins: {
        legend: {
            display: false
        }
    }
};


const radarCtx = document.getElementById('radarChart').getContext('2d');
const myRadarChart = new Chart(radarCtx, {
    type: 'radar',
    data: radarData,
    options: radarOptions
});

const barCtx = document.getElementById('barChart').getContext('2d');
const myBarChart = new Chart(barCtx, {
    type: 'bar',
    data: barData,
    options: barOptions
});